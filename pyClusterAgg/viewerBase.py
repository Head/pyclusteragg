#
# Basic 3D viewer using PyOpenGL and GLFW; no use of GLUT in this version.
#


#
# Imports.
#

# Standard imports
import math


# Import OpenGL; needed; do not import any GLUT.
_haveOpenGL = True
try:
    import OpenGL
    from OpenGL.GL import *
    from OpenGL.GLU import *
    import glfw

except ImportError as err:
    _haveOpenGL = False
    print( "Failed to load OpenGL library  (even without any GLUT extensions) and GLFW; will not be able to visualise the results [in viewerBase]." )
    print( " - error was: {}".format(err) )

# Need PIL for image output only.
_havePIL = True
try:
    from PIL import Image
except ImportError as err:
    _havePIL = False
    print( "Failed to load PIL image library; will not be able to save images [in viewerBase]." )


#
# Main class definition.
#
class viewerBase:

    #
    # Constructor.
    #
    def __init__( self, boxDims, width=640, height=480, windowTitle="viewerBase; see terminal message for controls", backgroundRGB=[0.8,0.8,0.8] ):
        """Initialises a GLFW window. Must set the box dimensions. Cal also set the window title, the width and height."""

        # Initialise GLFW.
        if not glfw.init():
            print( "Could not initialise GLFW - call to init() failed." )
            return
        
        # Create a window.
        self._window = glfw.create_window( width, height, windowTitle, None, None )         # Last two arguments are 'monitor' and 'share'.
        if not self._window:
            print( "Could not open a GLFW window of the requested dimension." )
            glfw.terminate()
            return
        
        # Store the box dimensions. From these, get a maximum length scale.
        self._X, self._Y, self._Z = boxDims
        self._L = math.sqrt( self._X**2 + self._Y**2 + self._Z**2 )

        # Initialise the view parameters.
        self._centreX  = self._X/2
        self._centreY  = self._Y/2
        self._centreZ  = self._Z/2
        self._eyeR     = 1.25 * self._L
        self._eyeTheta = math.pi/2
        self._eyePhi   = math.pi/2

        # Used for rotating image when the mouse button is depressed.
        self._applyRotation  = False
        self._lastMousePos_x = None
        self._lastMousePos_y = None

        # Define the background colour. Defaults to light grey.
        self._backgroundRGB = backgroundRGB

		# Default factors for mouse and keyboard interaction; these could all be made variable
        self._scrollFactor 			=	1e-1				# Using keyboard to move the centre
        self._mouseRotationFactor	=	5e-3				# Using mouse to rotate view
        self._keyboardZoomSpeed     =	0.1					# Using keyboard to zoom in/out

        # Make the window's context the current one.
        glfw.make_context_current( self._window )

        # OpenGL: smooth lines and polygons.
        glEnable( GL_LINE_SMOOTH    )
        glEnable( GL_POLYGON_SMOOTH )
        glEnable( GL_COLOR_MATERIAL )

        # OpenGL: Depth testing.
        glClearDepth( 1.0           )
        glEnable    ( GL_DEPTH_TEST )
        glDepthFunc ( GL_LEQUAL     )

        # OpenGL: Hints.
        glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST )

        # Set callback functions.
        glfw.set_key_callback( self._window, self._keyCallback)
        glfw.set_mouse_button_callback( self._window, self._mouseCallback )

        # Display commands.
        self.displayCommands()

    #
    # Destructor.
    #
    def __del__( self ):
        """Destructor. Closes the window and terminates GLFW. Checks glfw still exists before calling either of these routines."""
        try:
            glfw.destroy_window( self._window )
            glfw.terminate()
        except:
            pass 

    #
    # Set up and accessors / modifiers.
    #

    def displayCommands( self ):
        """Prints commands to the command line. If overridden by a child class, should still call this method first."""
        print( "Commands:" )
        print( "'q', 'ESC' :\tQuit" )
        print( "'[', ']'   :\tZoom in/out" )
        print( "<cursors>  :\tScroll laterally." )
        print( "<mouse>    :\tRotate view (with left button depressed)." )
        print( "'s'        :\tSave image to a JPEG file." )

    def setWindowTitle( self, newTitle ):
        """Sets the window title."""
        glfw.set_window_title( self._window, newTitle )

    # Direct access/control of view configuration
    def getCentre( self ):
        """Returns [x,y,z] of the focus centre."""
        return [ self._centreX, self._centreY, self._centreZ ]

    def getEye( self ):
        """Returns the [R,theta,phi] of the eye (spherical polars)."""
        return [ self._eyeR, self._eyeTheta, self._eyePhi ]

    def setCentreAndEye( self, centre, eye ):
        """Sets the centre and eye; pass two 3-lists, (x,y,z) for the centre followed
        by (R,theta,phi) for the eye. These are the sae format as the get...() methods."""

        if len(centre)!=3 or len(eye)!=3:
            print( "Cannot set centre and/or eye: One or both lists of wrong length." )
            return

        self._centerX = centre[0]
        self._centreY = centre[1]
        self._centreZ = centre[2]

        self._eyeR     = eye[0]
        self._eyeTheta = eye[1]
        self._eyePhi   = eye[2]

    
    #
    # Call back functions.
    #
    def _keyCallback( self, window, key, scancode, action, mode ):

        # Deal with cases for holding a key down first of all.
        if action==glfw.REPEAT or action==glfw.PRESS:

            # '[' to zoom out, ']' to zoom in.
            if key == glfw.KEY_LEFT_BRACKET:
                self._eyeR *= 1.0 + self._keyboardZoomSpeed

            if key == glfw.KEY_RIGHT_BRACKET:
                self._eyeR *= 1.0 - self._keyboardZoomSpeed
 
            # Cursor keys for lateral motion.
            if key == glfw.KEY_RIGHT:
                self._centreX += math.sin(self._eyePhi)*self._scrollFactor*self._X
                self._centreY -= math.cos(self._eyePhi)*self._scrollFactor*self._Y

            if key == glfw.KEY_LEFT:
                self._centreX -= math.sin(self._eyePhi)*self._scrollFactor*self._X
                self._centreY += math.cos(self._eyePhi)*self._scrollFactor*self._Y

            if key == glfw.KEY_UP:
                self._centreZ -= self._scrollFactor * self._Z

            if key == glfw.KEY_DOWN:
                self._centreZ += self._scrollFactor * self._Z

        # From now on, just deal with the first time a key is pressed.
        if action!=glfw.PRESS: return

        # 'q' or 'ESC' for quit.
        if key==glfw.KEY_Q or key==glfw.KEY_ESCAPE:
            glfw.set_window_should_close( self._window, True )
        
        # 's' to save current render as a jpeg file.
        if key==glfw.KEY_S:
            filename = "image.jpg"
            self.saveImage( filename )
            print( "Saved view to '{}'.".format(filename) )
  



    def _mouseCallback( self, window, button, action, mode ):
        """Call back function for the mouse, used to rotate the image when the button is depressed."""

        if button==glfw.MOUSE_BUTTON_LEFT:

            # If pressed, store the initial coordinates and set the 'rotating' flag.
            if action==glfw.PRESS:
                self._lastMousePos_x, self._lastMousePos_y = glfw.get_cursor_pos( self._window )
                self._applyRotation = True

	    	# If releasing just reset the flag.
            if action==glfw.RELEASE:
                self._applyRotation = False


    #
    # I/O
    #
    def saveImage( self, filename ):
        """Saves the image of the current render with the given filename (but always saves as JPEG!). Requires Image from PIL."""
	
        dummy1, dummy2, width, height = glGetDoublev( GL_VIEWPORT )
        data = glReadPixels( 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE )

        image = Image.frombytes( "RGB", (int(width),int(height)), data )
        image = image.transpose( Image.FLIP_TOP_BOTTOM )
        image.save( filename, "JPEG" )


    #
    # Rendering.
    #
    def mainLoop( self ):
        """The main loop. Call this to start the interaction with the user."""

        while not glfw.window_should_close( self._window ):
            self.displayAll()
    
    def displayAll( self ):
        """Displays the full state including clearning the screen and re-setting the view angle."""
        
        # Set up for rendering.
        self._preRender()

        # Display the system box as a wireframe.
        self._drawSystemBox()

        # The child class would normally override this routine.
        self.displayState()

        # Swap front and back buffers.
        glfw.swap_buffers(self._window)

        # Poll for, and process, events.
        glfw.poll_events()


    def displayState( self ):
        """This method is overriden by the child class to display whatever is contained within the system box.
        In the base class it does nothing, so only the wireframe box is displayed."""

        pass

    def _preRender( self ):
        """Sets upt for rendeing. Sets the viewing angle and location, background colour, and lighting."""

        # Set the background colour.
        glClearColor( self._backgroundRGB[0], self._backgroundRGB[1], self._backgroundRGB[2], 1.0 )
        glClear     ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT )

        # Set the perspective
        glMatrixMode( GL_PROJECTION )
        glLoadIdentity()

        viewPortParams = glGetDoublev( GL_VIEWPORT )        	# x, y, width, height
        aspectRatio = ( 1.0 if viewPortParams[3]==0.0 else viewPortParams[2]/viewPortParams[3] )
        gluPerspective( 45.0, aspectRatio, 0.25, 10*self._L )

        # Set view direction.
        glMatrixMode  ( GL_MODELVIEW )
        glLoadIdentity()
        gluLookAt(
            self._centreX + self._eyeR*math.sin(self._eyeTheta)*math.cos(self._eyePhi),		# Eye position
            self._centreY + self._eyeR*math.sin(self._eyeTheta)*math.sin(self._eyePhi),
            self._centreZ + self._eyeR*math.cos(self._eyeTheta),
            self._centreX,											                    # Centre
            self._centreY,
            self._centreZ,
            0.0,												           	            # Direction of 'up'
            0.0,
            1.0
        )

        # Rotate if mouse button is down. Will do nothing if mouse button not down.
        self._rotateWithMouse()

        # Initialise the lighting.
        glLightfv     ( GL_LIGHT0, GL_AMBIENT, (0.0, 0.0, 0.0, 1.0) )			# Default: (0,0,0,1)
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, (0.2, 0.2, 0.2, 1.0) )	    	# Default: (0.2,0.2,0.2,1.0)

        # Enable both light sources, which are sensible for most viewing options, if a little basic.
        glEnable( GL_LIGHT0 )
        glEnable( GL_LIGHTING )
 
    # Rotates the view given mouse drag.
    def _rotateWithMouse( self ):
        """Rotates the view as the mouse is dragged, but only if the left utton is down."""

        if not self._applyRotation: return

        # Get the current cursor position.
        x, y = glfw.get_cursor_pos( self._window )

        # Change from previous coordinates, stored persistently (and initialised when the button is first pressed).
        deltaX = x - self._lastMousePos_x
        deltaY = y - self._lastMousePos_y

        # Rotate accordingly.
        self._eyePhi   -= deltaX * self._mouseRotationFactor
        self._eyeTheta -= deltaY * self._mouseRotationFactor
	
        # Avoid values of theta close to zero or pi, as can get inversion.
        if self._eyeTheta < 0.01*math.pi: self._eyeTheta = 0.01*math.pi
        if self._eyeTheta > 0.99*math.pi: self._eyeTheta = 0.99*math.pi

        # Store the current position to allow the delta's in the next call to be calculated.
        self._lastMousePos_x = x
        self._lastMousePos_y = y

    # Draws the wireframe box.
    def _drawSystemBox( self ):
        """Draws the wireframe box."""
  
        # Use the wireframe box primitive, with a black line.
        glColor3f( 0, 0, 0 )
        self.drawWireframeBox( [0,0,0], [self._X,self._Y,self._Z], lineWidth=3 )

    #
    # Primitives.
    #

    # Wireframe box.
    def drawWireframeBox( self, coords, edges, lineWidth=1 ):
        """Draws a wireframe box with the given (x,y,z) coordinates for a corner, and (length,width,breadth) corresponding to
        x, y and z respectively. Can optionallly specify the line width (defaults to 1). Does not set colour lighting type.
        No normals are defined, so will change colour uniformly given the light source."""

        # Sanity check.
        if len(coords) != len(edges):
            print( "Cannot draw box; coordinates and edge length vectors not of the same length." )
            return
        
        # Set the line width.
        glLineWidth( lineWidth )

        # Short hands,
        x, y, z = coords
        X, Y, Z = edges

        # Use four faces of the solid rectangle, so a little over-specified.
        glBegin( GL_LINE_LOOP )
        glVertex( x+X, y, z   )
        glVertex( x+X, y, z+Z )
        glVertex( x  , y, z+Z )
        glVertex( x  , y, z   )
        glEnd()

        glBegin( GL_LINE_LOOP )
        glVertex3f( x  , y+Y, z   )
        glVertex3f( x+X, y+Y, z   )
        glVertex3f( x+X, y+Y, z+Z )
        glVertex3f( x  , y+Y, z+Z )
        glEnd()

        glBegin( GL_LINE_LOOP )
        glVertex3f( x+X, y  , z   )
        glVertex3f( x+X, y+Y, z   )
        glVertex3f( x+X, y+Y, z+Z )
        glVertex3f( x+X, y  , z+Z )
        glEnd()

        glBegin( GL_LINE_LOOP )
        glVertex3f( x, y  , z   )
        glVertex3f( x, y+Y, z   )
        glVertex3f( x, y+Y, z+Z )
        glVertex3f( x, y  , z+Z )
        glEnd()
    
    # Rectangular slab, with a cube as a special case.
    def drawRectangularSlab( self, coords, edges ):
        """Draws a solid rectangular box with the given (x,y,z) coordinates for a corner, and (length,width,breadth) corresponding to
        x, y and z respectively. Does not set colour lighting type."""

        # Sanity check.
        if len(coords) != len(edges):
            print( "Cannot draw box; coordinates and edge length vectors not of the same length." )
            return

        # Short hands,
        x, y, z = coords
        X, Y, Z = edges

        # The 6 faces.
        glBegin( GL_QUADS )

        glNormal3f( 0, -1, 0 )              # Set the current normal to be that for this face, before specifying the vertex coordinates.
        glVertex( x+X, y, z   )
        glVertex( x+X, y, z+Z )
        glVertex( x  , y, z+Z )
        glVertex( x  , y, z   )

        glNormal3f( 0, 1, 0 )
        glVertex3f( x  , y+Y, z   )
        glVertex3f( x+X, y+Y, z   )
        glVertex3f( x+X, y+Y, z+Z )
        glVertex3f( x  , y+Y, z+Z )

        glNormal3f( 1, 0, 0 )
        glVertex3f( x+X, y  , z   )
        glVertex3f( x+X, y+Y, z   )
        glVertex3f( x+X, y+Y, z+Z )
        glVertex3f( x+X, y  , z+Z )

        glNormal3f( -1, 0, 0 )
        glVertex3f( x, y  , z   )
        glVertex3f( x, y+Y, z   )
        glVertex3f( x, y+Y, z+Z )
        glVertex3f( x, y  , z+Z )

        # These last two are the top and bottom faces that were not required for the wireframe box.
        glNormal3f( 0, 0, 1 )
        glVertex3f( x  , y  , z+Z )
        glVertex3f( x+X, y  , z+Z )
        glVertex3f( x+X, y+Y, z+Z )
        glVertex3f( x  , y+Y, z+Z )

        glNormal3f( 0, 0, -1 )
        glVertex3f( x  , y  , z )
        glVertex3f( x+X, y  , z )
        glVertex3f( x+X, y+Y, z )
        glVertex3f( x  , y+Y, z )

        glEnd()

    # Draw a sphere.
    def drawSphere( self, centre, radius, stacks=5, slices=10 ):
        """Draws a sphere with the given centre and radius, without using the GLUT sphere primitive. 'stacks' and 'slices' set the vertical
        and horizontal (really, azimuthal and polar) resolution respectively."""

        # Shorthand.
        x, y, z = centre

        glBegin( GL_TRIANGLES )

        # Modified from biofilmWindow.cpp
        for j in range(stacks):
            v  =  j    / stacks
            v1 = (j+1) / stacks

            for i in range(slices):
                u  =  i    / slices
                u1 = (i+1) / slices

                self.__singleSphereVertex( radius, u , v , x, y, z )
                self.__singleSphereVertex( radius, u , v1, x, y, z )
                self.__singleSphereVertex( radius, u1, v1, x, y, z )

                self.__singleSphereVertex( radius, u , v , x, y, z )
                self.__singleSphereVertex( radius, u1, v1, x, y, z )
                self.__singleSphereVertex( radius, u1, v , x, y, z )

        glEnd()

    def __singleSphereVertex( self, r, u, v, x, y, z ):
        """Used by drawSphere() for each vertex in the list of triangles."""

        # Spherical polar angles.
        theta = u * 2*math.pi
        phi   = v *   math.pi

        # Coordinates of this vertex. Normal first so it is the current normal when the vertex is specified.
        glNormal3f( math.cos(theta)*math.sin(phi), math.sin(theta)*math.sin(phi), math.cos(phi) )
        glVertex3f( x + r*math.cos(theta)*math.sin(phi), y + r*math.sin(theta)*math.sin(phi), z + r*math.cos(phi) )
 



#
# Example usage by a derived class.
#
if __name__ == "__main__":

    # Define the class.
    class exampleViewer( viewerBase ):

        # Override this class to draw whatever is relevant the the problem.
        def displayState( self ):

            # Testing: Wireframe rectangular box, solid rectangular box, and a sphere.
            glColor3f( 0, 1, 0 )
            self.drawWireframeBox( [1,2,3], [3,2,1], lineWidth=5 )

            glColor3f( 1, 0, 0 )
            self.drawRectangularSlab( [4,5,6], [1,2,3] )
            glColor3f( 0, 0, 0 )
            self.drawWireframeBox( [4,5,6], [1,2,3], lineWidth=1 )

            glColor3f( 0, 0, 1 )
            self.drawSphere( [1,1,1], 1 )
    
    # Create an instance and start running.
    view = exampleViewer( [10,10,10] )
    view.mainLoop()