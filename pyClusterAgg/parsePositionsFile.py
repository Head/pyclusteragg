#
# Class that parses a single saved .csv files from the cluster aggregation model.
# Factory methods to extract dictionaries, keyed by index where possible and filename
# when not, of sequences of states taken from a standard indexing notation, or from
# an archive file.
#

#
# Imports.
#
import os
import tarfile
import io
import sys


class parsePositionsFile:

    def __init__( self, fnameOrReader ):
        """Creates a single positions state from the given file. Assumes file output from C-code as per late December 2020."""

        # Initialise state variables.
        self._numParticles = None
        self._boxWidth     = None
        self._positions    = []
        self._clusters     = []

		# Convert filename to readable object; if fails, assume it was a reader (e.g. an open file) in the first place.
        try:
            fileIn = open( fnameOrReader, 'r' )
        except TypeError:
            fileIn = io.TextIOWrapper( fnameOrReader )
	
        # First line ends with the system size.
        self._boxWidth = int( fileIn.readline().strip().split()[-1] )

        # Second line ends with the number of particles.
        self._numParticles = int( fileIn.readline().strip().split()[-1] )

        # Next line is blank.
        fileIn.readline()

        # Now have one line per particle, each of which gives its (x,y,z) coordinates separated by commas.
        for n in range(self._numParticles):
            x, y, z = [ int(coord) for coord in fileIn.readline().strip().split(",") ]
            self._positions.append( [ x,y,z] )

        # Next line is blank.
        fileIn.readline()

        # Now have an unknown number of clusters, one line per cluster, containing all of the particle indices in that cluster.
        while True:
            line = fileIn.readline().strip()
            if not line: break      # Empty line; assume end of clusters (may also be end of file)

            self._clusters.append( [ int(index) for index in line.split() ] )

    #
    # Properties; just getters so far.
    #
    @property
    def boxWidth( self ):
        return self._boxWidth
    
    @property
    def numParticles( self ):
        return self._numParticles
    
    @property
    def numClusters( self ):
        return len( self._clusters )
    
    #
    # Other accessors.
    #
    def coordsOfParticle( self, n ):
        if n<0 or n>=self.numParticles:
            raise IndexError( "Bad particle index; must be in the range 0 <= (index) < {}.".format(self.numParticles) )

        return self._positions[n]
    
    def sizeOfCluster( self, c ):
        if c<0 or c>=self.numClusters:
            raise IndexError( "Bad cluster index; must be in the range 0 <= (index) < {}.".format(self.numClusters) )
        return len(self._clusters[c])
    
    def particleIndexForCluster( self, c, n ):
        if n<0 or n>=self.sizeOfCluster(c):
            raise IndexError( "Bad particle index {0} for cluster {1}.".format(n,c) )        
        return self._clusters[c][n]
    
    def maxClusterSize( self ):
        return max( [self.sizeOfCluster(c) for c in range(self.numClusters)] )
        

    #
    # Pretty print some information about a single state.
    #
    def prettyPrint( self ):
        """Prints some information about the state, with ellipses when the information is too extensive to display sensibly."""
 
        # Box geometry and number of particles and clusters.
        print( "Cubic periodic box with edge length {}.".format(self.boxWidth) )
        print( "Box contains {} particle(s) organised into {} cluster(s).".format(self.numParticles,self.numClusters) )

        # Just the first 10 particle coordinates.
        print()
        for n in range(10):
            xyz = self.coordsOfParticle(n)
            print( "Coordinates of particle {0}:\t({1[0]},{1[1]},{1[2]})".format(n,xyz))
        print( "...\n" )

        # Print particle indices for at most the first 5 clusters, with at most 5 indices for each cluster.
        for clust in range(min(5,self.numClusters)):
            print( "Cluster index {0} has {1} particle(s) with indices:\t".format(clust,self.sizeOfCluster(clust)), end="" )

            for n in range( min(5,self.sizeOfCluster(clust)) ):
                print( self.particleIndexForCluster(clust,n), end=" ")
            if self.sizeOfCluster(clust) > 5: print( "...", end="" )
            print()

        if( self.numClusters>5 ):
            print( "..." )




    #
    # Factory methods.
    #

    @staticmethod
    def singleState( filename ):
        """Returns an instance of singleState for the given filename; just a wrapper for the constructor."""
        return parsePositionsFile(filename)

    @staticmethod
    def localFiles( filename ):
        """Returns a dictionary of parsePositionsFile objects, where the key is either just the given filename, or (if an index
        can be inferred from the filename), all such files in the sequence, keyed by their index."""

        #
        # Try to extract the index from the given filename.
        #

        # Location of the first dot; assume index comes just before this.
        indexExt = filename.find(".")
        if indexExt == -1:
            print( "No extension in '{}'; cannot determine index.".format(filename) )
            return
        
        # How many digits for the index?
        numDigits = 0
        while numDigits<indexExt and filename[indexExt-numDigits-1].isdigit():
            numDigits += 1

        # If there are no digits, just take the file name as the sole key.
        if numDigits==0:
            return { filename: parsePositionsFile(filename) }

        #
        # Loop through all possible indices given the number of digits.
        #
        states = {}
        for index in range(10**numDigits):

            trialFileName = "{0}{1:0>{2}}{3}".format( filename[:indexExt-numDigits], index, numDigits, filename[indexExt:] )
            if os.path.isfile( trialFileName ):
                states[index] = parsePositionsFile(trialFileName)

        return states
    

    @staticmethod
    def fromArchive( archiveName ):
        """Extracts multiple states from an archive using Python's tarfile module, and return a dictionary for each state it
        was possible to extract, with the filename or index as key. Any index is assumed to follow the same format as the
        indexedStates() factory method."""

        # All states that could be extracted from the archive.
        states = {}

        # Use the tarfile module to open up the archive.
        arc = tarfile.open( archiveName )

        # Loop through each file in the archive.
        for file in arc.getmembers():

            # Get the file reader from the archive.
            fileObj = arc.extractfile( file.name )

            # Try to convert to a save state file. If failed, just assume it was not a saved state file.
            try:
                saveObj = parsePositionsFile.singleState( fileObj )
            except Exception as err:
                continue

            # Try to find a suitable key; index if possible, the filename if not.
            key = None
            try:

                # Similar to indexedStates().
                indexExt, numDigits = file.name.find("."), 0
                while numDigits<indexExt and file.name[indexExt-numDigits-1].isdigit():
                    numDigits += 1

                key = int( file.name[indexExt-numDigits:indexExt] )

            except:

                # Could not extract index, so just use the filename.
                key = file.name

            # Add to the dictionary
            states[key] = saveObj

        return states

    #
    # Other static methods.
    #

    @staticmethod
    def getKey( filename, states ):
        """Attempts to convert 'trialKey' to an actual key in the dicionoary of states, as return by a factor method."""

        # Default to the key being the full filename.
        key = filename

        # Location of the first dot; assume index comes just before this.
        indexExt = filename.find(".")
        if indexExt != -1:

            # How many digits for the index?
            numDigits = 0
            while numDigits<indexExt and filename[indexExt-numDigits-1].isdigit():
                numDigits += 1
            
            # Get the index if there were any digits.
            if numDigits:
                key = int( filename[indexExt-numDigits:indexExt] )

        # See if the key exists in the list of states.
        if not key in states.keys():
            raise KeyError( "Could not find key (index or filename) '{}' in extracted states.".format(trialKey) )

        return key

