#
# Extracts pore positions and sizes from a given configuration of spherical particles.
#
# Call file to see a test (at the end).

#
# Imports
#

# Standard imports
import math
import numpy
import copy



class poreConfiguration:

	def __init__( self, XYZ, periodic=False, verbose=False ):
		"""poreConfiguration attempts to determine the approximate locations and sizes of pores
		given a configuration of spherical particles. Initialise with the box dimensions (currently
		assumes 2D or 3D and rectangular), and a True/False for periodicity (periodicity-per-dimension
		not yet supported).
		"""
		
		# Sanity check
		if len(XYZ)<2 or len(XYZ)>3:
			print( "ERROR: poreConfiguration:: only supports 2D or 3D." )
			return

		# Store initialisation parameters
		self._dim      = len(XYZ)
		self._XYZ      = numpy.array(XYZ)
		self._periodic = periodic
		self._verbose  = verbose
		
		# Persistent storage of pore positions and radii. Overwritten with each call to "determineConfiguration()".
		self._porePosns = []
		self._poreRadii = []
		
		# Can optionally store the pore positions and radii prior to the merging step; can be
		# used for visualisation/demonstration purposes.
		self._preMergePosns = []
		self._preMergeRadii = []

	def determineConfiguration( self, posns, radii, b, storeOriginal=False ):
		"""Determines the pore configuration for the given positions and radii of particles.
		After the final removal of overlapping particles (always the smallest is removed,
		starting from the smallest in the list), stores positions and radii internally,
		which can be accessed using allPorePosns(), allPoreRadii(), and numPores().
		If 'storeOriginal' is True, will also store the pore configuration prior to merging,
		which can then be accessed using preMergePores() [ tuple of positions and radius ].
		"""

		# Sanity check
		if len(posns) != len(radii):
			print( "ERROR: Position and radii lists of different lengths [in poreConfiguration.determineConfiguration()]" )
			return

		# Determine the unit cell sizes in each dimension, rounding to nearest
		numX = int( 0.5 + self._XYZ[0]/b )
		numY = int( 0.5 + self._XYZ[1]/b )
		if self._dim == 3:
			numZ = int( 0.5 + self._XYZ[2]/b )

		dX = self._XYZ[0] / numX
		dY = self._XYZ[1] / numY
		if self._dim==3:
			dZ = self._XYZ[2] / numZ
		
		# Convert the particle positions and radii into a suitably-shaped numpy array: (N,dim) for positions, (N) for radii
		POSNS = numpy.array( posns )
		RADII = numpy.array( radii )
		
		# Create a lattice of putative pore centres.
		lattice = numpy.zeros( ([numX,numY,numZ] if self._dim==3 else [numX,numY]), dtype=float )
		if self._verbose:
			if self._dim==2:
				print( "poreConfiguration: 2D {0}x{1} lattice, single cell {2}x{3}.".format(numX,numY,dX,dY) )
			else:
				print( "poreConfiguration: 3D {0}x{1}x{2} lattice, single cell {3:.3g}x{4:.3g}x{5:.3g}.".format(numX,numY,numZ,dX,dY,dZ) )
		
		#
		# Step 1: For each lattice point, find the closest particle (negative overlap). Slow; could be accelerated with a cell list.
		#
		if self._verbose: print( "poreConfiguration: Populating lattice with smallest negative overlaps with particles." )
		if self._dim == 2:
			for i in range(numX):
				for j in range(numY):
					porePosns[i,j] = self._smallestNegativeOverlap( numpy.array([i*dX,j*dY]), POSNS, RADII )
		else:
			for i in range(numX):
				for j in range(numY):
					ROW = numpy.array( [ [i*dX,j*dY,k*dZ] for k in range(numZ) ] )
					lattice[i,j,:] = self._smallestNegativeOverlap_vectorised( ROW, POSNS, RADII )
#					for k in range(numZ):				# Original version; in tests, only about 10% slower than the vectorised one.
#						porePosns[i,j,k] = self._smallestNegativeOverlap( numpy.array([i*dX,j*dY,k*dZ]), POSNS, RADII )


		#
		# Step 2: Convert to persistent lists of pore sizes and positions, sorted in order of largest first
		#
		if self._verbose: print( "poreConfiguration: Mapping lattice to sorted lists, removing pores smaller than the lattice spacing." )
		self._porePosns, self._poreRadii = [], []
		if self._dim == 2:
			for i in range(numX):
				for j in range(numY):
					if lattice[i,j] > 0.5*b:
						self._porePosns.append( [i*dX,j*dY]	 )				# NB: Using numpy arrays here can cause sorted() problems below
						self._poreRadii.append( lattice[i,j] )
		else:
			for i in range(numX):
				for j in range(numY):
					for k in range(numZ):
						if lattice[i,j,k] > 0.5*b:
							self._porePosns.append( [i*dX,j*dY,k*dZ] )		# Again, numpy arrays here can cause sorted() problems below
							self._poreRadii.append( lattice[i,j,k] )

		# Order in terms of radii, smallest last (so the largest, and least likely to be deleted, are near the start)
		self._poreRadii, self._porePosns = [ list(tuple) for tuple in zip( *sorted(zip(self._poreRadii,self._porePosns),reverse=True) ) ]
		
		# Convert each position to a numpy array now (not before sorted(), as it seems to fail for large arrays)
		for i in range(len(self._porePosns)): self._porePosns[i] = numpy.array( self._porePosns[i] )
		
		# Optionally store this pre-merge pore configuration
		if storeOriginal:
			self._preMergePosns = copy.copy( self._porePosns )
			self._preMergeRadii = copy.copy( self._poreRadii )

		#
		# Step 3: Remove/merge overlapping pores (always remove the smallest, starting from the smallest).
		#
		if self._verbose: print( "poreConfiguration: Merging overlapping pores." )

		POSNS, RADII = numpy.array(self._porePosns), numpy.array(self._poreRadii)
		
		n = len(self._poreRadii)-1				# Start from the smallest
		while n >= 1:
			
			SEPNS = POSNS[:n,:] - self._porePosns[n]
			if self._periodic:
				SEPNS += 0.5   * self._XYZ
				SEPNS  = SEPNS % self._XYZ
				SEPNS -= 0.5   * self._XYZ

			DISTS = numpy.sqrt(numpy.sum(SEPNS*SEPNS,1)) - RADII[:n] - self._poreRadii[n]
			
			if numpy.any( numpy.less(DISTS,0.0) ):
				del( self._poreRadii[n] )
				del( self._porePosns[n] )		
		
			n -= 1
		
			
	# Returns the closest distance from the given point to all particles, taking into account periodic BCs (if any)
	# Assumes positions and radii of all particles in the form of a single numpy.array() each, and uses numpy arrays
	# throughout to vectorise the calculations, dramatically improving speed.
	def _smallestNegativeOverlap( self, xyz, POSNS, RADII ):
	
		# Create an (N,dim) array of positions minus the location of the lattice node.
		SEPNS = POSNS - numpy.array(xyz)
		
		# Implement the periodic BCs. Per-dimension periodicity would ise "for k in (dim); SEPNS[:,k] = SEPNS[:,k] % self._XYZ[k]"
		if self._periodic:
			SEPNS += 0.5   * self._XYZ							# Add half of the box dimensions to each separation vector
			SEPNS  = SEPNS % self._XYZ
			SEPNS -= 0.5   * self._XYZ							# Restore the half-box step

		# Get the lengths of each of the vectors. Can simplify if all radii equal, but makes very little difference to speed.
		DIST = numpy.sqrt( numpy.sum(SEPNS*SEPNS,1)	)			# Sum over the within-particle indices
		
		# Subtract off the radii of the particles
		DIST -= RADII
		
		# Return the minimum distance
		return numpy.min(DIST)

	# Vectorised version of _smallestNegativeOverlap(); faster, but uses more memory.
	def _smallestNegativeOverlap_vectorised( self, VEC, POSNS, RADII ):
	
		# Create an (M,N,dim) array of VEC minus POSNS, where M is the size of VEC
		SEPNS = VEC[:,numpy.newaxis,:] - POSNS[numpy.newaxis,:,:]

		# Periodic BCs
		if self._periodic:
			SEPNS += 0.5   * self._XYZ
			SEPNS  = SEPNS % self._XYZ
			SEPNS -= 0.5   * self._XYZ

		# Get the lengths of each vector
		DIST = numpy.sqrt( numpy.sum(SEPNS*SEPNS,2) )			# Note now sum is now over axis 2
		
		# Subtract off the radii 
		DIST -= RADII[numpy.newaxis:,]
		
		# Return with a vector of minimum distances
		return numpy.min(DIST,1)
		
			
	#
	# Accessors
	#
	
	# The number of pores
	def numPores( self ):
		return len(self._porePosns )
	
	# Pore positions as a single list of numpy.array()'s
	def allPorePosns( self ):
		return self._porePosns

	# Pore radii as a list of floats
	def allPoreRadii( self ):
		return self._poreRadii

	# Pre-merge configuration, if stored
	def preMergePores( self ):
		return [ self._preMergePosns, self._preMergeRadii ]
	
	# Construct a histogram from the last-determined pore configuration. Specify max and bin width.
	def poreSizePDF( self, min_r, max_r, dr ):
	
		# Sanity check
		if dr <= 0.0 or max_r <= min_r:
			print( "Cannot create pore size distribution; bad bin parameters [in poreConfiguration::poreSizeDist()]" )
			return [ None, None ]

		# Number of bins		
		numBins = int( 1 + (max_r-min_r)/dr )

		# Initialise the axes
		rVals = [ min_r + i*dr for i in range(numBins) ]
		pVals = [ 0 ] * numBins
		
		# Construct the probability density function
		for n in range(self.numPores()):
			bin = int( ( self.allPoreRadii()[n] - min_r ) / dr )
			if bin>=0 and bin<numBins:
				pVals[bin] += 1.0 / dr

		return [ rVals, pVals ]
		
	

#
# Unit test; if called from command line
#
if __name__ == "__main__":

	# Parameters for this test
	X, Y, Z = 20.0, 20.0, 20.0						# Box geometry
	N = 400											# Number of particles in the system configuration
	r = 1.0											# The radius of each particle (assumed constant here),
	
	# Create the poreConfiguration object
	pores = poreConfiguration( [X,Y,Z], periodic=True, verbose=True )
	
	# Generate a random configuration of particles.
	posns, radii = [], []
	for n in range(N):
		posns.append( numpy.array([X,Y,Z])*numpy.random.random( (3,) ) )
		radii.append( r )
	
	# Determine the pore configuration, i.e. positions and radii of all pores.
	pores.determineConfiguration( posns, radii, 1.2*r )

	# Visualise the results using the basicViewer base class. Also need direct access to PyOpenGL.
	import basic3DViewer
	import OpenGL 
	from OpenGL.GL import *
	from OpenGL.GLU import *
#	from OpenGL.GLUT import *
	
	# Derive a class from the basic viewer to draw the particles and the pores
	class poreConfigCheck( basic3DViewer.basic3DViewer ):
	
		def __init__( self, posns, radii, poreConfig ):
			super(poreConfigCheck,self).openWindow( [X,Y,Z], windowTitle="[', ']' to zoom",
												wireframeBox=True, backgroundColourRGB=(1.0,1.0,1.0) )
			self._posns = posns
			self._radii = radii
			self._pores = pores
			
			self._displayParticles = True
			self._displayPores     = True
		
			print( "c: toggle particle configuration, p: toggle pore configuration." )

		def _drawSystemState( self ):

			# Resolution of the spheres
			slices, stacks = 10, 10

			# Draw the particles in green
			if self._displayParticles:
				self._setMaterialWithColour([0.0,1.0,0.0])
				for n in range(N):
					glPushMatrix()				
					try:
						glTranslatef   (self._posns[n][0],self._posns[n][1],self._posns[n][2])
						glutSolidSphere(self._radii[n],slices,stacks)
					finally:
						glPopMatrix()
				
			# Draw the pores in red
			if self._displayPores:
				posns, radii = self._pores.allPorePosns(), self._pores.allPoreRadii()
				self._setMaterialWithColour([1.0,0.0,0.0])
				for p in range( self._pores.numPores() ):
					glPushMatrix()				
					try:
						glTranslatef   (posns[p][0],posns[p][1],posns[p][2])
						glutSolidSphere(radii[p],slices,stacks)
					finally:
						glPopMatrix()
		
		# Toggle display of particles and/or pores
		def keyPressed( self, *args ):

			if args[0] == b"c": self._displayParticles = not self._displayParticles
			if args[0] == b"p": self._displayPores     = not self._displayPores

			super(poreConfigCheck,self).keyPressed(*args)
	
	# Create the viewer object and run
	viewer = poreConfigCheck( posns, radii, pores )
	viewer.run()
				

	
	
		
		