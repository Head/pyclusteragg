#
# Viewer for a saved state from the cluster aggregation code. Requires 'basic3DViewer.py.
#


#
# Imports.
#

# Class that parses the state files has some useful static methods.
from . import parsePositionsFile

# Parent class is part of the same package.
from . import viewerBase

# Also calculate and display the pores using this class.
from . import poreConfiguration

# From PyOpenGL. Note that GLUT is no longer used, but viewerBase has some useful primitives.
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *

# For direct acces to GLFW commands.
import glfw


class viewer_OpenGL_GLFW( viewerBase.viewerBase ):

    def __init__( self, states, startState=None ):
        """Initialises the viewer with the states to display. Can also optionally select a key for the state to start from."""

        # Store the states and their keys.
        self._states = states
        self._keys   = list( states.keys() )

        # The initial key is either the one provided (if valid), or the first one on the list.
        self._stateIndex = 0
        self._currentKey = self._keys[0]
    
        if startState:
            try:
                self._currentKey = parsePositionsFile.getKey(startState,states)
            except Exception as err:
                print( err )
                self._currentKey = self._keys[0]

        # Assume the box dimensions are the same for all states, so take the value from the first one.
        L = self._states[self._keys[0]].boxWidth

        # Initialise the base class with the dimensions of the cubic box. Will open the GLFW window.
        super().__init__( [L]*3, backgroundRGB=(1.0,1.0,1.0) )

        # Set window title to reflect the current state.
        self._setWindowTitle()

        # Can also display pores, but only if selected by the user as can be very slow.
        self._poreObj = None


    def displayCommands( self ):
        """Extend commands sent to terminal to include those specific to this application."""
        super().displayCommands()
        print( "'i'        :\tDisplay information about the current state to the terminal" )
        print( "'n', 'p'   :\tNext / Previous states according to the key list." )
        print( "'s'        :\tSave current image as a .jpg file." )
        print( "'m'        :\tGenerate a movie, saving images for each state." )
        print( "'o'        :\tCalculate and display pores. Warning: Can be slow." )

    def displayState( self ):
        """Draws the system state corresponding to the current key. """

        # Current state, where 'state' is an instance of the class in 'parsePositionsFile'.
        state    = self._states[self._currentKey]
        maxClust = state.maxClusterSize()               # Size of largest cluster; may be more than one.

        # Loop over all clusters.
        for clust in range( state.numClusters ):

            # Size of the largest cluster; can be used to modify what is displayed.
            clustSize = state.sizeOfCluster(clust)

            # Loop over all particle indices within this cluster.
            for index in range( state.sizeOfCluster(clust) ):

                # Convert to the actual particle index, i.e. one that is understood outside of its containing cluster.
                n = state.particleIndexForCluster( clust, index )

                # Get the coordinates of this particle.
                coords = state.coordsOfParticle(n)

                #
                # Display particles as a solid cube and/or as a wireframe. Can decide how to do this based in
                # e.g. the cluster size ('clustSize'), if the cluster equals the largest one in size ('clustSize==maxSize).
                #

                # For this example, only display solid cubes for particles in a cluster that is (a) >1 in size,
                # and (b) matches the largest cluster in size. Note that there may be more than one such cluster.
                if clustSize>1 and clustSize==maxClust:
                    glColor3f( 0, 1, 0 )                                    # 'RGB', so e.g. "0,1,0" means red=0, green=1, blue=0, i.e. pure green.
                    self.drawRectangularSlab( coords, [1,1,1] )             # Coordinates of a corner, and edge lengths along each direction.

                # For all particles, irrespective of size, show a black wireframe around the cube edges.
                # Could also choose when to display this wireframe ... ?
                glColor3f( 0, 0, 0 )
                self.drawWireframeBox( coords, [1,1,1] ) 

        # If pores have been calculated for this state, also display them.
        if self._poreObj:

            # Set the colour for the pores.
            glColor3f( 1, 0, 0 )

            # Draw each pore as a sphere.
            for pore in range(self._poreObj.numPores()):
                self.drawSphere( self._poreObj.allPorePosns()[pore], self._poreObj.allPoreRadii()[pore], stacks=10, slices=20 )


    def _setWindowTitle( self ):
        """Sets the window title to reflect the current key."""
        super().setWindowTitle( "Displaying state with key (index/filename) '{}'.".format(self._currentKey) )

    def _keyCallback( self, window, key, scancode, action, mode ):
        """Extends the range of options for keypress; calls the parent class's method if not understood."""

        # Only if the key has been pressed the first time.
        if action == glfw.PRESS:

            # If the 'i' key is pressed, display information about the state.
            if key == glfw.KEY_I:
                print( "Some information about the currently displayed state, with key={}:".format(self._currentKey) )
                self._states[self._currentKey].prettyPrint()
                return
        
            # If either the 'n' or 'p' keys are pressed, try to move to the n(ext) or p(revious) state as per the key list.
            if key==glfw.KEY_N or key==glfw.KEY_P:
                try:
                    self._stateIndex += ( +1 if key==glfw.KEY_N else -1 )
                    self._currentKey  = self._keys[self._stateIndex]
                    self._setWindowTitle()
                    self._poreObj = None
                except:
                    print( "No more states to display" )
                return

            # Save file with name 'key.png', up to the first ',' if key was a filename.
            if key == glfw.KEY_S:
                fname = "{}.jpg".format( str(self._currentKey).split(".")[0] )
                self.saveImage( fname )
                print( "Saved current image as '{}'.".format(fname) )
                return

            # Calculate and display the pores.
            if key == glfw.KEY_O:
                self.calculatePores()
                return

            # Generate images for all keys that can be converted to a movie using e.g. ffmpeg.
            if key == glfw.KEY_M:

                # DO not display pores during the movie, as it would be very slow to calculate for each frame.
                self._poreObj = None
 
                # Get the current centre and eye.
                centre = self.getCentre()
                eye    = self.getEye   ()

                # Loop through all states, saving and rotating slightly as we go.
                print( "Generating images for all integer keys..." ) 
                for key in self._keys:
                    if not isinstance(key,int): continue        # Skip non-integer keys.

                    # Set the state and view angle.
                    self._currentKey = key
                    eye[2] -= 0.05                                  # Rotate by a small fraction of a radian per frame.
                    self.setCentreAndEye( centre, eye )

                    # Display the new state.
                    self.displayAll()

                    # Save to a suitable filename that includes leading zeros.
                    self.saveImage( "{:0>4}.jpg".format(key) )

                print( "... finished." )
                return

        # If still here, call the parent class's method instead.
        super()._keyCallback( window, key, scancode, action, mode )
    
    #
    # Calculating and displaying pores.
    #
    def calculatePores( self ):
        """Calculates the pores and displays to terminal and in the viewer. Can be very slow."""

        # Set up the pore object.
        self._poreObj = poreConfiguration.poreConfiguration( [self._X,self._Y,self._Z], periodic=True, verbose=False )

        # Take all particles to have a radius of 0.5
        print( "Calculating pores for current state ..." )

        state = self._states[self._currentKey]
        self._poreObj.determineConfiguration( state._positions, [0.5]*state._numParticles, 1.0 )

        # Print information to terminal.
        print( "Found {0} pores with radii (largest to smallest): {1}".format(self._poreObj.numPores(),self._poreObj.allPoreRadii()) )


