#
# Parses the saved .csv files from the cluster aggregation model.
#

#
# Imports.
#

# Standard python imports.
import os
import argparse
import sys

# The package specific to the state files output by the C-code.
import pyClusterAgg


#
# Parse command line arguments to decide what to do.
#
parser = argparse.ArgumentParser( description="Parses position .csv files output by the cluster aggregation code" )
parser.add_argument( "filename", help="File name of the positions .csv or .tar.gz archive to parse" )
args = parser.parse_args()

# Extract from archive, or try to extract indexed files with the same format as the given one.
if args.filename.endswith( ".tar.gz" ):
    states = pyClusterAgg.parsePositionsFile.fromArchive( args.filename )
else:
    states = pyClusterAgg.parsePositionsFile.localFiles( args.filename )

# List all states extracted.
print( "Extracted {0} states with keys: {1}.".format(len(states)," ".join([str(key) for key in states.keys()]) ))

if not len(states):
    print( "Could not extract any states." )
    sys.exit(-1)

#
# View all extracted states using pyOpenGL and GLFW. The commands for controlling the viewer should be printed to the terminal when it is launched.
#
viewer = pyClusterAgg.viewer_OpenGL_GLFW( states, startState=(None if args.filename.endswith(".tar.gz") else args.filename) )
viewer.mainLoop()




